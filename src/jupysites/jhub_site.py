import jinja2
import logging
import yaml
import os
import stat
from dataclasses import dataclass, asdict

dump_dir = "deployments"
auto_nfs_server = "nas12.ethz.ch"
auto_nfs_name = "auto-data"
auto_nfs_dest = "/data"
auto_nfs_share_root = "/fs1202/id_eduit_jhub_customers_prod"

file_loader = jinja2.PackageLoader(__package__)
env = jinja2.Environment(loader=file_loader)
# some lists must be merged, add here
merged_lists = ["extra_env"]


def read_sites(config_dir):
    """
    Read a directory with subdirectories containing <moodle_id>/config.yml site configurations and create
    a 'Sites' object with all sites discovered.
    :param config_dir: Directory to search
    :return site: Sites object
    """
    sites = Sites()
    for item in os.listdir(config_dir):
        site = None
        site_path = os.path.join(config_dir, item)
        if os.path.isdir(site_path):
            site = read_site(site_path)
            if site is not None:
                sites[int(site.moodle_id)] = site

    return sites


def read_site(config_dir):
    """
    Read a single site configuration file and create a 'Site' object
    :param config_dir: Directory containing the 'config.yml' for a JupyterHub site
    :return: 'Site' object
    """
    f = None
    try:
        f = open(os.path.join(config_dir, "config.yml"))
    except FileNotFoundError:
        return
    except Exception as e:
        logging.error(e)
        exit(1)
    site = yaml.load(f, yaml.Loader)
    f.close()
    # Optional empty values are ignored by YAML import, must be added here
    if not hasattr(site, "external_mounts"):
        site.external_mounts = []
    if not hasattr(site, "extra_env"):
        site.extra_env = []

    return site


class Sites(dict):
    """
    Dict based object holding all JupyterHub sites of a given semester.
    """

    def write(self, config_root):
        """
        Write a site configuration file. Writes to (and creates, if not exists) a directory named after the Moodle ID
        and a 'config.yml'.
        :param config_root: Directory designated to store configurations.
        """
        for site_key in self.keys():
            self[site_key].write(config_root)

    def create_site(self, site_id, course_id, course_title):
        """
        Create a new site (not yet writing it, call 'write' to save)
        :param site_id: Moodle ID
        :param course_id: Course ID as used in VVZ
        :param course_title: Course title
        """
        if site_id in self.keys():
            logging.error("Site already exists: {0}".format(site_id))
            exit(1)
        self[site_id] = Site(site_id, course_id, course_title)

    def print(self, defaults, verbose=False, customized=False):
        """
        Print formatted information to console.
        :param defaults: object holding the defaults for this semester, being combined with the site configuration.
        :param verbose: print most of available data
        :param customized: print only sites which have customized attributes, compared to defaults
        """
        for site_id in self.keys():
            self[site_id].print(defaults, verbose, customized)


@dataclass
class Site:
    """
    Jupyter site configuration object.
    """

    moodle_id: int
    images: list
    image_pull_policy: str
    external_mounts: list
    extra_env: list
    netpol_blacklist: list
    semester_state: str
    cluster: str = ""
    domain: str = ""
    compat_domain: str = ""
    course_id: str = ""
    course_title: str = ""
    ticket_url: str = ""
    default_ui: str = ""
    home_storage_class: str = ""
    home_size: int = 0
    db_storage_class: str = ""
    memory_guarantee: int = 0
    memory_limit: int = 0
    cpu_guarantee: int = 0
    cpu_limit: int = 0
    jhub_chart_version: str = ""
    jhub_image: str = ""
    jhub_image_tag: str = ""
    moodleapi_image: str = ""
    moodleapi_image_tag: str = ""
    moodleapi_transfer_limit: int = 0
    moodleapi_auth_user: str = ""
    moodleapi_auth_key: str = ""
    lti_key: str = ""
    lti_secret: str = ""
    auto_nfs: bool = False
    shared_volume: bool = False
    idle_timeout: int = 900
    announcement: str = ""

    def __init__(
        self,
        moodle_id: int,
        course_id: str,
        course_title: str,
        image_pull_policy: str = "IfNotPresent",
        default_ui: str = "",
        cluster: str = "",
        domain: str = "",
        compat_domain: str = "",
        home_storage_class: str = "",
        home_size: int = 0,
        db_storage_class: str = "",
        memory_guarantee: int = 0,
        memory_limit: int = 0,
        cpu_guarantee: float = 0,
        cpu_limit: float = 0,
        jhub_chart_version: str = "",
        jhub_image: str = "",
        jhub_image_tag: str = "",
        moodleapi_image: str = "",
        moodleapi_image_tag: str = "",
        moodleapi_transfer_limit: int = 0,
        moodleapi_auth_user: str = "",
        moodleapi_auth_key: str = "",
        lti_key: str = "",
        lti_secret: str = "",
        extra_env: list = [],
        auto_nfs: bool = False,
        shared_volume: bool = False,
        idle_timeout: int = 900,
        announcement: str = "",
    ):
        self.moodle_id = moodle_id
        self.cluster = cluster
        self.domain = domain
        self.compat_domain = compat_domain
        self.course_id = course_id
        self.course_title = course_title
        self.image_pull_policy = image_pull_policy
        self.default_ui = default_ui
        self.home_storage_class = home_storage_class
        self.dbStorageClass = db_storage_class
        self.memory_guarantee = memory_guarantee
        self.memory_limit = memory_limit
        self.cpu_guarantee = cpu_guarantee
        self.cpu_limit = cpu_limit
        self.jhub_chart_version = jhub_chart_version
        self.jhub_image = jhub_image
        self.jhub_image_tag = jhub_image_tag
        self.lti_key = lti_key
        self.lti_secret = lti_secret
        self.moodleapi_image = moodleapi_image
        self.moodleapi_image_tag = moodleapi_image_tag
        self.moodleapi_transfer_limit = moodleapi_transfer_limit
        self.moodleapi_auth_user = moodleapi_auth_user
        self.moodleapi_auth_key = moodleapi_auth_key
        self.home_size = home_size
        self.images = []
        self.external_mounts = []
        self.extra_env = extra_env
        self.netpol_blacklist = []
        self.auto_nfs = auto_nfs
        self.shared_volume = shared_volume
        self.idle_timeout = idle_timeout
        self.announcement = announcement

    def merge(self, defaults, mark_defaults=False):
        """
        Combine individual site configuration and semester defaults. Individual attributes override defaults.
        :param defaults: object with the set of defaults for this semester
        :param mark_defaults: for print output, mark items overridden with an asteriks
        :return: site configuration object with overrides applied
        """
        d = asdict(defaults)
        customized = False
        for key in d:
            if isinstance(d[key], list):
                if not hasattr(self, key) or self.__getattribute__(key) == []:
                    self.__setattr__(key, d[key])
                else:
                    # not all list may be merged
                    if key in merged_lists:
                        item_list = self.__getattribute__(key)
                        for item in d[key]:
                            item_list.append(item)
                    customized = True

            elif isinstance(d[key], int) or isinstance(d[key], float):
                if hasattr(self, key):
                    if self.__getattribute__(key) == 0:
                        self.__setattr__(key, d[key])
                    else:
                        customized = True
                        if mark_defaults:
                            self.__setattr__(key, str(self.__getattribute__(key)) + "*")
                else:
                    self.__setattr__(key, d[key])
            else:
                if hasattr(self, key):
                    if self.__getattribute__(key) == "":
                        self.__setattr__(key, d[key])
                    else:
                        customized = True
                        if mark_defaults:
                            self.__setattr__(key, self.__getattribute__(key) + "*")

                else:
                    self.__setattr__(key, d[key])
        return customized

    def write(self, config_root):
        """
        Save configuration for this site.
        :param config_root: root directory of configuration database
        """
        f = None
        dir_path = os.path.join(config_root, str(self.moodle_id))
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        config_path = os.path.join(dir_path, "config.yml")
        # Does config already exists?
        try:
            f = open(config_path, mode="r")
            logging.error("Cowardly refusing to overwrite an existing configuration")
            exit(1)
        except PermissionError:
            logging.error("No permission to read {0}".format(config_path))
            exit(1)
        except FileNotFoundError:
            pass  # we expect this
        try:
            f = open(config_path, mode="w")
        except PermissionError:
            logging.error("No permission to write {0}".format(config_path))
            exit(1)
        yaml.dump(self, f)
        f.close()

    def print(self, defaults, verbose=False, customized_only=False):
        is_customized = self.merge(defaults=defaults, mark_defaults=True)
        if not customized_only or (customized_only and is_customized):
            if verbose:
                print(env.get_template("display_site_verbose.j2").render(asdict(self)))
                print("=========================================")
            else:
                print(
                    "{0} {1} {2}".format(
                        self.moodle_id, self.course_id, self.course_title
                    )
                )

    def dump(self, config_root, defaults):
        """
        Create configuration files for deployment. Handles exeption and directory creation, if necessary
        :param config_root: root directory of configuration database
        :param defaults: object containing semester defaults
        """

        def safe_write(dir, file_name, data, make_executable=False):
            f = None
            try:
                f = open(os.path.join(dir, output_path, file_name), "w")
            except Exception as e:
                logging.error("Could open config file: {0}".format(e))
                exit(1)
            try:
                f.write(data)
            except AttributeError as e:
                logging.error(e)
                exit(1)
            f.close()
            if make_executable:
                # Shell scripts for deployment and removal
                try:
                    os.chmod(
                        os.path.join(dir, output_path, file_name),
                        stat.S_IRWXU
                        | stat.S_IRGRP
                        | stat.S_IXGRP
                        | stat.S_IROTH
                        | stat.S_IXOTH,
                    )
                except Exception as e:
                    logging.warning(e)

        # Main starts here
        # Dynamically add auto NFS share to external_mounts, if configured
        if self.auto_nfs:
            self.external_mounts.append(
                {
                    "name": auto_nfs_name,
                    "server": auto_nfs_server,
                    "path": "{0}/{1}".format(auto_nfs_share_root, self.moodle_id),
                    "dest": auto_nfs_dest,
                }
            )
        output_path = os.path.join(config_root, "deployments", str(self.moodle_id))
        if os.path.exists(output_path) and not os.path.isdir(output_path):
            logging.error("Destination not a directory: {0}".format(output_path))
            exit(1)
        if not os.path.exists(output_path):
            try:
                os.makedirs(output_path)
            except Exception as e:
                logging.error(e)
                exit(1)

        # Mix in defaults
        self.merge(defaults)

        # dump all configuration files and deployment scripts
        output = env.get_template("info.yml.j2").render(asdict(self))
        safe_write("", "INFO.yml", output)
        output = env.get_template("values.yml.j2").render(asdict(self))
        safe_write("", "values.yml", output)
        output = env.get_template("kustomization.yml.j2").render(asdict(self))
        safe_write("", "kustomization.yml", output)
        output = env.get_template("namespace.yml.j2").render(asdict(self))
        safe_write("", "namespace.yml", output)
        output = env.get_template("pvc.yml.j2").render(asdict(self))
        safe_write("", "pvc.yml", output)
        output = env.get_template("deploy_hub.sh.j2").render(asdict(self))
        safe_write("", "deploy_hub.sh", output, make_executable=True)
        output = env.get_template("remove_hub.sh.j2").render(asdict(self))
        safe_write("", "remove_hub.sh", output, make_executable=True)
        try:
            os.makedirs(
                os.path.join(
                    config_root, "deployments", str(self.moodle_id), "moodle-api"
                ),
                exist_ok=True,
            )
        except Exception as e:
            logging.error(e)
            exit(1)
        output = env.get_template("moodle-api_kustomization.yml.j2").render(
            asdict(self)
        )
        safe_write("", os.path.join("moodle-api", "kustomization.yml"), output)
        output = env.get_template("moodle-api_ingress.yml.j2").render(asdict(self))
        safe_write("", os.path.join("moodle-api", "ingress.yml"), output)
        output = env.get_template("moodle-api_volume.yml.j2").render(asdict(self))
        safe_write("", os.path.join("moodle-api", "volume.yml"), output)
        output = env.get_template("moodle-api_env-vars.yml.j2").render(asdict(self))
        safe_write("", os.path.join("moodle-api", "env-vars.yml"), output)

        print("Wrote {0}".format(self.moodle_id))

    def create(self, config_root, site_id):
        pass
