from importlib.metadata import version

__version__ = version("jupysites")

import click
import logging

# import legacy_db
from .semester import Semester

perma_sites_dir = "permanent_hubs"


@click.group()
@click.version_option(package_name="jupysites")
def cli():
    """
    Manage configurations for JupyterHub sites.
    """
    pass


@click.command()
@click.option("--site", "-s", help="Limit output to site", default=None)
@click.option("--verbose", "-v", help="Detailed output", default=False, is_flag=True)
@click.option(
    "--customized", "-c", help="Customized sites only", default=False, is_flag=True
)
@click.argument("semester")
def list(semester, site, verbose, customized):
    """
    List sites.

    \b
    semester: semester to be listed
    """
    config = Semester(semester)
    if site is not None:
        config.filter(site, None)
        config.print(verbose=verbose)
    else:
        config.print(verbose=verbose, customized=customized)
        # collect semester independent hubs
        config = Semester(perma_sites_dir)
        config.print(verbose=verbose, customized=customized)


@click.command()
@click.argument("semester")
@click.option(
    "--output_dir",
    "-o",
    help="Configuration root where folder 'deployments' will be created (default '.'",
    default=".",
)
@click.option("--site", "-s", help="Dump this site (ID) only", default=None)
def dump(semester, output_dir, site):
    """
    Create deployment(s)

    \b
    semester: target semester as 23FS, 23HS etc.
    """
    dumped = False
    try:
        config = Semester(semester)
        config.dump(output_dir, site)
        dumped = True
    except FileNotFoundError:
        pass
    try:
        config = Semester(perma_sites_dir)
        config.dump(output_dir, site)
        dumped = True
    except FileNotFoundError:
        pass

    if dumped is False:
        logging.error("Site {0} not found!".format(site))
        exit(1)


@click.command()
@click.argument("semester")
@click.argument("site")
@click.argument("course_id")
@click.argument("course_title")
def create(semester, site, course_id, course_title):
    """
    Create a new JupyterHub site

    \b
    semester: target semester as 23FS, 23HS etc.
    site: Moodle ID using the JupyterHub site
    course_id: VVZ id as e.g. 123-4567-89L (use 000-0000-00 for demo, 111-1111-11 for unofficial courses)
    course_title: official course title (see VVZ)
    """
    config = Semester(semester)
    config.create_course(site, course_id, course_title)


cli.add_command(list)
# cli.add_command(convert)
cli.add_command(dump)
cli.add_command(create)

if __name__ == "__main__":
    cli()
