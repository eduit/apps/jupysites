import logging
import os
import yaml
from dataclasses import dataclass


def read_defaults(config_dir):
    """
    Read semester defaults and create 'defaults' object containing all attributes
    :param config_dir: configuration directory for this semester
    :return: 'defaults' object
    """
    f = None
    try:
        f = open(os.path.join(config_dir, "defaults.yml"))
    except Exception as e:
        logging.error(e)
        exit(1)

    defaults = yaml.load(f, Loader=yaml.Loader)
    f.close()
    return defaults


@dataclass
class Defaults(yaml.YAMLObject):
    """
    Contains all attributes used as defaults for a JupyterHub site.
    """

    images: list
    image_pull_policy: str
    netpol_blacklist: list
    extra_env: list
    memory_guarantee: int
    memory_limit: int
    cpu_guarantee: int
    cpu_limit: int
    home_storage_class: str
    home_size: int
    db_storage_class: str
    jhub_chart_version: str
    jhub_image: str
    jhub_image_tag: str
    moodleapi_image: str
    moodleapi_image_tag: str
    default_ui: str
    cluster: str
    domain: str
    compat_domain: str
    lti_key: str
    lti_secret: str
    moodleapi_auth_user: str
    moodleapi_auth_key: str
    moodleapi_transfer_limit: int
    semester_state: str = "active"

    def __init__(self, config=None, semester=""):
        # init for legacy DB: one large configuration.
        # No 'config' or 'semester' arguments are needed for standard usage.
        if config is not None and semester == "":
            # YAML dump recognizes only attributes set here, not as class defaults
            if "memory_guarantee" in config:
                self.memory_guarantee = config["memory_guarantee"]
            else:
                self.memory_guarantee = 256
            if "memory_limit" in config:
                self.memory_limit = config["memory_limit"]
            else:
                self.memory_limit = 1024
            if "image_pull_policy" in config:
                self.image_pull_policy = config["image_pull_policy"]
            else:
                self.image_pull_policy = "IfNotPresent"
            if "cpu_guarantee" in config:
                self.memory_guarantee = config["cpu_guarantee"]
            else:
                self.memory_guarantee = 500
            if "cpu_limit" in config:
                self.cpu_limit = config["cpu_limit"]
            else:
                self.cpu_limit = 1000
            if "home_storage_class" in config:
                self.home_storage_class = config["home_storage_class"]
            else:
                self.home_storage_class = "nfs-idsd"
            if "db_storage_class" in config:
                self.db_storage_class = config["db_storage_class"]
            else:
                self.db_storage_class = "nfs-idsd"
            if "jhub_chart_version" in config:
                self.jhub_chart_version = config["jhub_chart_version"]
            else:
                self.jhub_chart_version = "1.2.0"
            if "jhub_image" in config:
                self.jhub_image = config["jhub_image"]
            else:
                self.jhub_image = "registry.ethz.ch/k8s-let/images/k8s-hub-lti"
            if "jhub_image_tag" in config:
                self.jhub_image_tag = config["jhub_image_tag"]
            else:
                self.jhub_image_tag = "1.2.0.0"
            if "moodleapi_image" in config:
                self.moodleapi_image = config["moodleapi_image"]
            else:
                self.moodleapi_image = "bengig/jupyterhub-fileserver-api"
            if "moodleapi_image_tag" in config:
                self.moodleapi_image_tag = config["moodleapi_image_tag"]
            if "moodleapi_transfer_limit" in config:
                self.moodleapi_transfer_limit = config["moodleapi_transfer_limit"]
            else:
                self.moodleapi_image_tag = "1.0.0"
            if "default_ui" in config:
                self.default_ui = config["default_ui"]
            else:
                self.default_ui = "/lab"
            if "domain" in config:
                self.domain = config["domain"]
            else:
                self.domain = "ethz.ch"
            if "compat_domain" in config:
                self.compat_domain = config["compat_domain"]
            if "cluster" in config:
                self.cluster = config["cluster"]
            else:
                self.cluster = "let-02"
            self.lti_key = config["lti"]["key"]
            self.lti_secret = config["lti"]["secret"]
            self.moodleapi_auth_user = config["moodle_api_auth_user"]
            self.moodleapi_auth_key = config["moodle_api_auth_key"]
            self.semester_state = "active"

        else:
            logging.error(
                "Either semester or complete legacy config required for defaults generation"
            )
            exit(1)

    def write(self, config_dir):
        """
        Write the configuration for this default. Mainly used for conversion of old type of site configuration
        :param config_dir: directory containing all sites for a semester
        """
        f = None
        file_path = os.path.join(config_dir, "defaults.yml")
        try:
            f = open(file_path, mode="w")
        except PermissionError:
            logging.error("No permission to write {0}".format(file_path))
            exit(1)
        yaml.dump(self, f)
