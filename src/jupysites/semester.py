import os
import os.path
import logging
from .jhub_site import read_sites
from .defaults import read_defaults


class Semester:
    """
    JupyterHub configurations for one semester. Includes sites and a set of defaults, which can be overwritten
    per site.
    """

    name = ""
    sites = None
    default = None
    base_dir = ""

    def __init__(self, name, sites=None, default=None):
        """
        Initialize a semester configuration. If 'sites' and 'defaults' are None, tries to read from standard
        configuration directory (env variable JHUB_CONFIG_DIR)
        :param name: Semester name, i.e. 23FS, 23HS (number first for better sorting)
        :param sites: sites list (jhub_site.sites)
        :param default: default set (defaults.defaults)
        """
        self.name = name
        base_dir = os.getenv("JHUB_CONFIG_DIR", ".")

        # Test if base directory exists. Abort if not or not valid.
        if not os.path.exists(base_dir):
            logging.error("Configuration base path {0} does not exist".format(base_dir))
            exit(1)
        if not os.path.isdir(base_dir):
            logging.error(
                "Configuration base path {0} is not a directory".format(base_dir)
            )
            exit(1)

        # Test if semester configuration directory exists.
        self.config_dir = os.path.join(base_dir, self.name)
        if not os.path.exists(self.config_dir):
            logging.error(
                "Configuration path {0} does not exist.".format(self.config_dir)
            )
            exit(1)
        if not os.path.isdir(self.config_dir):
            logging.error(
                "Configuration path {0} exists, but is not a directory".format(
                    self.config_dir
                )
            )
            exit(1)

        # Read defaults and site configurations
        if default is None:
            self.default = read_defaults(os.path.join(base_dir, name))
        else:
            self.default = default
        if sites is None:
            self.sites = read_sites(os.path.join(base_dir, name))
        else:
            self.sites = sites

    def write(self):
        """
        Write configuration files in $JHUB_CONFIG_DIR/<semester name>
        """
        self.default.write(self.config_dir)
        self.sites.write(self.config_dir)

    def read(self, semester):
        """
        Read configuration files from $JHUB_CONFIG_DIR/<semester name>
        """
        self.default = read_defaults(config_dir=self.config_dir)
        self.sites = read_sites(self.config_dir)

    def print(self, mode="normal", verbose=False, customized=False):
        """
        Show configurations in human-readable form
        :param mode:
        :param verbose: Print all information
        :param customized: Show only customized sites
        """
        self.sites.print(self.default, verbose=verbose, customized=customized)

    def dump(self, config_root, site=None):
        """
        Write deployments, deploy and remove scripts etc.
        :param config_root: Root path of deployment configurations
        :param site: Create files for this site only
        """
        if site is not None:
            try:
                self.sites[int(site)].dump(config_root, self.default)
            except KeyError:
                raise FileNotFoundError
        else:
            for site_id in self.sites:
                self.sites[site_id].dump(config_root, self.default)

    def create_course(self, site, course_id, course_title):
        """
        Create a new site configuration
        :param site: ID (Moodle ID) of new site
        :param course_id: Course ID as in VVZ (123-4567-89L)
        :param course_title: Title of the course
        """
        self.sites.create_site(site, course_id, course_title)
        self.sites[site].write(self.config_dir)

    def filter(self, site=None, pattern=None):
        """
        Restrict printed output to specified site
        :param site: ID of site
        :param pattern: unused
        """
        if site is not None:
            try:
                s = self.sites[site]
            except KeyError:
                logging.error("Site {0} not found".format(site))
                exit(1)
            self.sites.clear()
            self.sites[site] = s
