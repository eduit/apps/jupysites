# Management Tool for LET JupyterHub Sites

## Purpose
JupyterHub sites need several configuration objects to be deployed on Kubernetes:

 - `values.yml` for the main Helm chart
 - physical volume claim (PVC) for the home directories
 - namespace definition
 - deployment, ingress and environment variables for the Moodle API server

This tool creates, for individual sites, the complete collection of these files,
together with a deployment and a deletion script.

The sources for the site configurations are a defaults file, valid for a semester, and a YAML file
containing the distinct configuration attributes for a site. This makes changes to i.e. the default
notebook image tag easy. Defaults may be overridden in a site configuration, keeping flexibility.

The deployment and removal scripts created take care of all components, allowing for quick
changes of site configurations.

## Preparations
The deployments created require a few preparations to be applicable.

### kubectl
Has to be configured to allow access to the Kubernetes cluster. Before actually deploying something, the
script displays information about the cluster addressed by the current context, to be safe.

### Helm
Attach the Helm repository for JupyterHub first; or update it if not used for some time:
```shell
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update
```

## Architecture
Configurations are held per semester in the current directory, or in the directory `$JHUB_CONFIG_DIR`.
Semester configuration directories should be named `22HS`, `23FS` for a sorted appearance in the
filesystem. `23FS` will be used as an example for the topics below.

Additionally, a directory `permanent_hubs` may contain hubs which do not follow semester schedules, as
with test and demonstration hubs. The directory will be created automatically if it does not exist, but
it may remain optionally empty. However, it must contain a `defaults.yml` (see below). Best practice is
to create a symbolic link to the `defaults.yml` of the current semester.

The `kustomize` resource path of the Moodle assignment API is currently fixed and points to a certain
directory, a clone of the base repository. Therefore, the recommended directory structure is

```
<repo root>
+- k8s-jupyter-base-master
+- jupysites
+-- permanent_hubs
+--- defaults.yml (symlink to ../23HS/defaults.yml)
+-- 23FS
+--- defaults.yml
+-- 23HS
+--- defaults.yml
```


### Defaults
A file `23FS/defaults.yml` contains the defaults for the given semester. An example can be found in the
`examples` directory of this repository.

Defaults are valid, as long as they are not overriden in site specific configurations. Site values of 0, "0", [] or ""
are considered unset and do not override defaults.

The `compat_domain` helps to migrate the Moodle API server names: in contrast to the individual hub addresses,
the API names are configured globally in Moodle, by use of a pattern. Setting this option causes the API ingress
to be configured with two ingresses, one for a deprecated name and one for the upcoming new nae.

### Sites
Sites are stored in directories named after the Moodle ID. If needed, any file may be placed in this directory,
i.e. licenses, notes and additional information etc.

Configuration is held in this directory in the file `config.yml`. Only a few attributes are mandatory:

```yaml
!!python/object:jupysites.jhub_site.Site
course_id: 233-1142-00L
course_title: Example Course
moodle_id: 12345
```

For test or demo sites, we use dummy course IDs: 000-0000-00 for tests, 111-1111-11 for demo sites.

Individual attributes can be copied over from the defaults. The next example
- increases the memory limit (in MB without unit)
- uses a dedicated image
- creates a NFS mount for the default NAS data dir (to be provisioned separately)

```yaml
!!python/object:jupysites.jhub_site.Site
course_id: 233-1142-00L
course_title: Example Course
moodle_id: 12345
memory_limit: 2048
images:
  - description: Special environment for Example Course
    display_name: Special environment
    image: registry.ethz.ch/k8s-let/notebooks/jh-notebook-superspecial
    tag: 3.0.0-12-a
```

## Usage
```shell
Usage: jupysites [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  create   Create a new JupyterHub site
  dump     Create deployment(s)
  list     List sites.
  version
```
Further help is available for the commands by `jupysites <command> --help`.

For convenience, change the directory to `<repo_root>/jupysites` first. Site configurations
will be searched in `./<semester_dir>`, deployments written to `./deployments`.

### create
Creates a new configuration directory for a JupyterHub site and writes a minimal configuration.

The configuration contains a lot of empty fields, result of the Python object dump. It is recommended to
remove all fields except the ones mentioned above, mandatory for a minimal configuration. Otherwise, the
site will be listed as containing overrides.

### list
Displays information about site(s) on the terminal:

 - short list
 - full details
 - filter to display sites with overrides only
 - filter to display a specific site

### dump
Creates the configuration(s) in a subdirectory `deployments`, a subdirectory per site.

To deploy or update a JupyterHub site, switch to the correct `kubectl` cluster context if necessary, change to the site
directory created and run the deployment script:

```
kubectl config use-context let-04
cd deployments/12345
./deploy.sh
```

## Development
After checking out the repository, create an editable installation from the repo root directory with

```shell
pip install -e .
```
